
# Licensed under MIT License
#
# Indicated author and assumed copyright holder is:
#   Sainnhe Park
#
# Ported to Geany and tweaked by Kaden Flores <kdnfgc(at)protonmail(dot)com>
#

[theme_info]
name=Gruvbox-Dark
description=A modified version of Gruvbox. The contrast is adjusted to be softer in order to protect developers' eyes.
# incremented automatically, do not change manually
version=1
author=primejade
url=https://github.com/sainnhe/gruvbox-material
# list of each compatible Geany release version
compat=1.22;1.23;1.23.1;1.24

[named_colors]


current_line_black=#3c3836
bg=#282828
red=#fb4934
green=#b8bb26
orange=#fe8019
yellow=#fabd2f
blue=#83a598
magenta=#d3869b
cyan=#8ec07c
white=#f9f5d7
almost_white=#a89984
selection_grey=#665c54

[named_styles]
default= white;bg;false;false;
error=white;red;false;true
# Editor styles
#-------------------------------------------------------------------------------

selection=;selection_grey;false;true
current_line=;current_line_black;true
brace_good=white;selection_grey;true
brace_bad=error
margin_line_number=selection_grey
margin_folding=almost_white
fold_symbol_highlight=white
indent_guide=dark_grey
caret=white;;false
marker_line=yellow;very_dark_yellow
marker_search=black;dark_blue
marker_mark=light_green;lighter_black
call_tips=dark_grey;white;false;false
white_space=dark_grey;;true

# Generic programming languages
#-------------------------------------------------------------------------------

comment=selection_grey
comment_doc=comment
comment_line=comment
comment_line_doc=comment_doc
comment_doc_keyword=comment,bold
comment_doc_keyword_error=comment,italic

number=magenta
number_1=number
number_2=number_1

type=yellow
class=yellow
function=green,bold
parameter=cyan;

keyword=cyan
keyword_1=red
keyword_2=yellow
keyword_3=cyan
keyword_4=magenta

identifier=magenta
identifier_1=orange
identifier_2=green
identifier_3=blue
identifier_4=yellow

string=green
string_1=string
string_2=blue
string_3=red
string_4=default
string_eol=string_1,italic
character=cyan
backticks=string_2
here_doc=string_1

scalar=cyan
label=default,bold
preprocessor=red
regex=number_1
operator=default
decorator=string_1,bold
other=default
extra=keyword;

# Markup-type languages
#-------------------------------------------------------------------------------

tag=magenta
tag_unknown=tag,bold
tag_end=tag,bold
attribute=type
attribute_unknown=attribute,bold
value=number
entity=number

# Diff
#-------------------------------------------------------------------------------

line_added=green
line_removed=red
line_changed=preprocessor
