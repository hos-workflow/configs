[build-menu]
# %f will be replaced by the complete filename
# %e will be replaced by the filename without extension
# (use only one of it at one time)
FT_00_LB=_Compile
FT_00_CM=ruby -wc "%f"
FT_00_WD=
EX_00_LB=_Execute
EX_00_CM=ruby "%f"
EX_00_WD=
