[build-menu]
# %f will be replaced by the complete filename
# %e will be replaced by the filename without extension
# (use only one of it at one time)
FT_02_LB=_Lint
FT_02_CM=shellcheck --format=gcc "%f"
FT_02_WD=
EX_00_LB=_Execute
EX_00_CM="./%f"
EX_00_WD=
