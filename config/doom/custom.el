;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(custom-safe-themes
;;    '("88f7ee5594021c60a4a6a1c275614103de8c1435d6d08cc58882f920e0cec65e" default))
;;  '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(+bidi-arabic-face ((t `(:slant normal :weight normal :height 1.0 :width normal :foundry "PfEd" :family ,hosfamily))))
 '(+bidi-hebrew-face ((t `(:slant normal :weight normal :height 1.0 :width normal :foundry "PfEd" :family ,hosfamily))))
 '(markdown-header-face ((t (:inherit font-lock-function-name-face :weight bold :family "Source Sans Pro"))))
 '(markdown-header-face-1 ((t (:inherit markdown-header-face :height 2.0))))
 '(markdown-header-face-2 ((t (:inherit markdown-header-face :height 1.7))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :height 1.4))))
 '(markdown-header-face-4 ((t (:inherit markdown-header-face :height 1.2))))
 '(markdown-header-face-5 ((t (:inherit markdown-header-face :height 1.1))))
 '(markdown-header-face-6 ((t (:inherit markdown-header-face :height 1.0))))
 '(adoc-title-face ((t (:inherit font-lock-function-name-face :weight bold :family "Source Sans Pro"))))
 '(adoc-title-0-face ((t (:inherit adoc-title-face :height 2.0))))
 '(adoc-title-1-face ((t (:inherit adoc-title-face :height 1.7))))
 '(adoc-title-2-face ((t (:inherit adoc-title-face :height 1.5))))
 '(adoc-title-3-face ((t (:inherit adoc-title-face :height 1.3))))
 '(adoc-title-4-face ((t (:inherit adoc-title-face :height 1.1))))
 '(adoc-title-5-face ((t (:inherit adoc-title-face :height 1.0))))
 '(org-document-title ((t (:inherit outline-1 :height 2.0 :family "Source Sans Pro"))))
 '(org-level-1 ((t (:inherit outline-1 :height 2.0 :family "Source Sans Pro"))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.7 :family "Source Sans Pro"))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.5 :family "Source Sans Pro"))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.2 :family "Source Sans Pro"))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.0 :family "Source Sans Pro")))))
 ;; '(adoc-header-face ((t (:inherit font-lock-function-name-face :weight bold :family "Source Sans Pro"))))
 ;; '(asciidoc-header-face ((t (:inherit font-lock-function-name-face :weight bold :family "Source Sans Pro"))))
(put 'customize-face 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
