static const char *colors[SchemeLast][2] = {{
	/*                     fg         bg       */
	[SchemeNorm] = {{ "{color15}", "{color0}" }},
	[SchemeSel] = {{ "{color0}", "{color15}" }},
	[SchemeOut] = {{ "{color0}", "{color14}" }},
	[SchemeSelHighlight]  = {{ "{color0}", "{color1}" }},
	[SchemeNormHighlight] = {{ "{color0}", "{color14}" }},
}};
