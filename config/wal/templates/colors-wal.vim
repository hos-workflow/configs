" Special
let g:wal_wallpaper  = "{wallpaper}"
let g:wal_background = "{background}"
let g:wal_foreground = "{foreground}"
let g:wal_cursor     = "{cursor}"

" Colors
let g:wal_color0  = "{color0}"
let g:wal_color1  = "{color1}"
let g:wal_color2  = "{color2}"
let g:wal_color3  = "{color3}"
let g:wal_color4  = "{color4}"
let g:wal_color5  = "{color5}"
let g:wal_color6  = "{color6}"
let g:wal_color7  = "{color7}"
let g:wal_color8  = "{color8}"
let g:wal_color9  = "{color9}"
let g:wal_color10 = "{color10}"
let g:wal_color11 = "{color11}"
let g:wal_color12 = "{color12}"
let g:wal_color13 = "{color13}"
let g:wal_color14 = "{color14}"
let g:wal_color15 = "{color15}"
