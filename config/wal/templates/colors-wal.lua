M = {}

M.wal = {
	wal_wallpaper  = "{wallpaper}",
	wal_background = "{background}",
	wal_foreground = "{foreground}",
	wal_cursor     = "{cursor}",

	wal_color0  = "{color0}",
	wal_color1  = "{color1}",
	wal_color2  = "{color2}",
	wal_color3  = "{color3}",
	wal_color4  = "{color4}",
	wal_color5  = "{color5}",
	wal_color6  = "{color6}",
	wal_color7  = "{color7}",
	wal_color8  = "{color8}",
	wal_color9  = "{color9}",
	wal_color10 = "{color10}",
	wal_color11 = "{color11}",
	wal_color12 = "{color12}",
	wal_color13 = "{color13}",
	wal_color14 = "{color14}",
	wal_color15 = "{color15}",
}

return M
