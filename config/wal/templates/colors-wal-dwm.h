static const char normfgcolor[] = "{color15}";
static const char normbgcolor[] = "{color0}";
static const char normbordercolor[] = "{color8}";

static const char selfgcolor[] = "{color15}";
static const char selbgcolor[] = "{color2}";
static const char selbordercolor[] = "{color15}";

// static const char urg_fg[] = "{color15}";
// static const char urg_bg[] = "{color1}";
// static const char urg_border[] = "{color1}";
